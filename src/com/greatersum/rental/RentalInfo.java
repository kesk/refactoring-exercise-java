package com.greatersum.rental;

import com.greatersum.rental.model.*;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.Map;

public class RentalInfo {
    Map<String, Movie> movies;

    public RentalInfo(String moviesFilePath) throws FileNotFoundException {
        this(MovieStorage.loadMovies(moviesFilePath));
    }

    public RentalInfo(Map<String, Movie> movies) {
        this.movies = movies;
    }

    public String statement(Customer customer) {
        Receipt receipt = new Receipt(customer);
        for (MovieRental r : customer.getRentals()) {
            Movie movie = movies.get(r.getMovieId());
            BigDecimal price = calculateMoviePrice(movie.getCode(), r.getDays());
            int points = calculateFrequentRenterPoints(movie.getCode(), r.getDays());

            receipt.addReceiptItem(new ReceiptItem(movie, price));
            receipt.addFrequentRenterPoints(points);
        }

        return receipt.toString();
    }

    private static BigDecimal calculateMoviePrice(MovieCode code, int days) {
        BigDecimal amount = BigDecimal.valueOf(0);
        switch (code) {
            case REGULAR:
                amount = BigDecimal.valueOf(2);
                if (days > 2) {
                    amount = BigDecimal.valueOf((days - 2) * 1.5).add(amount);
                }
                break;

            case NEW:
                amount = BigDecimal.valueOf(days * 3);
                break;

            case CHILDRENS:
                amount = BigDecimal.valueOf(1.5);
                if (days > 3) {
                    amount = BigDecimal.valueOf((days - 3) * 1.5).add(amount);
                }
                break;
        }

        return amount;
    }

    private static int calculateFrequentRenterPoints(MovieCode code, int days) {
        int points = 0;
        switch (code) {
            case NEW:
                if (days > 2) {
                    points++;
                }
                points++;
                break;

            case REGULAR:
            case CHILDRENS:
                points++;
                break;
        }

        return points;
    }
}
